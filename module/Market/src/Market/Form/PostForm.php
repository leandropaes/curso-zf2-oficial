<?php

namespace Market\Form;

use Zend\Form\Element\Select;
use Zend\Form\Element\Submit;
use Zend\Form\Element\Text;
use Zend\Form\Form;

class PostForm extends Form
{
    private $categories;

    /**
     * @param field_type $categories
     */
    public function setCategories($categories)
    {
        $this->categories = $categories;
    }

    public function buildForm()
    {
        $this->setAttribute("method", "POST");

        $category = new Select('category');
        $category->setLabel('Category')
                 ->setValueOptions(
                     array_combine($this->categories, $this->categories)
                 )
                ->setAttributes(array('class' => 'form-control'));

        $title = new Text('title');
        $title->setLabel('Title')
              ->setAttributes(array('size' => 60, 'maxLength' => 128, 'class' => 'form-control'));

        $submit = new Submit('submit');
        $submit->setAttributes(array('value' => 'Post', 'class' => 'btn btn-primary'));

        $this->add($category)
             ->add($title)
             ->add($submit);
    }
}